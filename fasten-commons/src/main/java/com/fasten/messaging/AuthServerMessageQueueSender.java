package com.fasten.messaging;

import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.Session;
import javax.jms.TextMessage;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jms.core.JmsTemplate;
import org.springframework.jms.core.MessageCreator;
import org.springframework.stereotype.Component;

@Component
public class AuthServerMessageQueueSender {
	
	@Autowired
	JmsTemplate jmsQueueTemplate;

	public void sendMessage(final String json, final String nodeName) {
		jmsQueueTemplate.send(new MessageCreator() {
            @Override
            public Message createMessage(Session session) throws JMSException {
                TextMessage message = session.createTextMessage(json);
                message.setStringProperty("node", nodeName);
                return message;
            }
        });
	}
}
