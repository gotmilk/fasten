package com.fasten.messaging;

import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.Session;
import javax.jms.TextMessage;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jms.core.JmsTemplate;
import org.springframework.jms.core.MessageCreator;
import org.springframework.stereotype.Component;

@Component
public class AuthServerMessagePublisher {

	@Autowired
	JmsTemplate jmsTopicTemplate;

	public void sendMessage(final String json, final String nodeName) {
		jmsTopicTemplate.send(new MessageCreator() {
            @Override
            public Message createMessage(Session session) throws JMSException {
                TextMessage message = session.createTextMessage(json);
                message.setStringProperty("node", nodeName);
                return message;
            }
        });
		
	}
}
