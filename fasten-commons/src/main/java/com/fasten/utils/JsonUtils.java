package com.fasten.utils;

import java.io.IOException;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.fasterxml.jackson.databind.ObjectMapper;

@Component
public class JsonUtils {

	@Autowired
	private ObjectMapper jsonMapperObject;
	
	static final Logger logger = LoggerFactory.getLogger(JsonUtils.class);
	
	public ObjectMapper getJsonMapperObject() {
		return jsonMapperObject;
	}

	public void setJsonMapperObject(ObjectMapper jsonMapperObject) {
		this.jsonMapperObject = jsonMapperObject;
	}

	public Object deserializeJson(String json, Class<?> clazz) {
		try {
			return jsonMapperObject.readValue(json, clazz);
		} catch (IOException e) {
			logger.error("Cannot deserialize JSON", e);
		}
		return null;
	}

	public String serializeJson(Object o) {
		try {
			return jsonMapperObject.writeValueAsString(o);
		} catch (IOException e) {
			logger.error("Cannot serialize JSON", e);
		}
		return null;
	}

	@SuppressWarnings("unchecked")
	public static boolean jsonEquals(String json1, String json2) {
		ObjectMapper mapper = new ObjectMapper();
		try {
			Map<String, Object> m1 = (Map<String, Object>) (mapper.readValue(json1, Map.class));
			Map<String, Object> m2 = (Map<String, Object>) (mapper.readValue(json2, Map.class));
			return m1.equals(m2);
		} catch (Exception e) {
			logger.error("Cannot serialize JSON", e);
		}
		return false;
	}
	
	public static boolean isValidJSON(final String json){
	    boolean valid = true;
	    ObjectMapper mapper = new ObjectMapper();
	    
	    try{ 
	        mapper.readTree(json);
	    } catch(Exception e){
	        valid = false;
	    }
	    
	    return valid;
	}
}
