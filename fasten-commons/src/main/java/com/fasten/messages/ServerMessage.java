package com.fasten.messages;

import java.util.UUID;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonSubTypes;
import com.fasterxml.jackson.annotation.JsonTypeId;
import com.fasterxml.jackson.annotation.JsonTypeInfo;

public class ServerMessage {
	
	@JsonTypeId
	private String type;

	@JsonProperty("sequence_id")
	private UUID sequenceId;

	@JsonTypeInfo(use = JsonTypeInfo.Id.NAME, include = JsonTypeInfo.As.EXTERNAL_PROPERTY, property = "type")
	@JsonSubTypes({ 
		@JsonSubTypes.Type(value = ServerMessageLoginData.class, name = ServerMessageTypes.LOGIN_CUSTOMER),
		@JsonSubTypes.Type(value = ServerMessageTokenData.class, name = ServerMessageTypes.CUSTOMER_API_TOKEN),
		@JsonSubTypes.Type(value = ServerMessageErrorData.class, name = ServerMessageTypes.CUSTOMER_ERROR) })
	private ServerMessageData data;

	public ServerMessage() {
		this.sequenceId = UUID.randomUUID();
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public UUID getSequenceId() {
		return sequenceId;
	}

	public void setSequenceId(UUID sequenceId) {
		this.sequenceId = sequenceId;
	}

	public ServerMessageData getData() {
		return data;
	}

	public void setData(ServerMessageData data) {
		this.data = data;
	}

}
