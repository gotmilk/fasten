package com.fasten.messages;

public interface ServerMessageTypes {
	static final String LOGIN_CUSTOMER = "LOGIN_CUSTOMER";
	static final String CUSTOMER_API_TOKEN = "CUSTOMER_API_TOKEN";
	static final String CUSTOMER_ERROR = "CUSTOMER_ERROR";
	
}
