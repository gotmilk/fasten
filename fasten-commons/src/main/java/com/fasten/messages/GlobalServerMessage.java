package com.fasten.messages;

public class GlobalServerMessage {

	private String sessionId;
	private ServerMessage msg;
	
	public String getSessionId() {
		return sessionId;
	}

	public void setSessionId(String sessionId) {
		this.sessionId = sessionId;
	}

	public ServerMessage getMsg() {
		return msg;
	}

	public void setMsg(ServerMessage msg) {
		this.msg = msg;
	}
}
