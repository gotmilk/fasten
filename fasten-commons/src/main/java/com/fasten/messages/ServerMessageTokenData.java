package com.fasten.messages;

import java.util.Date;
import java.util.UUID;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonProperty;

public class ServerMessageTokenData extends ServerMessageData {
	@JsonProperty("api_token")
    private UUID apiToken;
    @JsonProperty("api_token_expiration_date")
    @JsonFormat(shape=JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd'T'HH:mm:ss'Z'")
    private	java.util.Date apiTokenExpirationDate;
    
    public UUID getApiToken() {
        return apiToken;
    }

    public void setApiToken(UUID apiToken) {
        this.apiToken = apiToken;
    }

    public Date getApiTokenExpirationDate() {
        return apiTokenExpirationDate;
    }

    public void setApiTokenExpirationDate(Date apiTokenExpirationDate) {
        this.apiTokenExpirationDate = apiTokenExpirationDate;
    }
}
