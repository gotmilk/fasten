# README #

Project containts Simple Websocket Server v1.0.

[Spring, JPA (EclipseLink), Jedis, JMS (using ActiveMQ) & etc.]

Test paths for Server:

* /server/active - currently active users
* /server/ - client

Currently:

* no DataAccessException/PersistenceException handling exists
* not for a production (e.g. datasources should be configured JNDI Resources, etc)
* no JUnit tests available
* I'm not using Spring Boot in that project

# DATABASE #
Autocreated JPA

```
#!sql

CREATE DATABASE `fasten`;

DROP TABLE IF EXISTS `USER`;
CREATE TABLE `USER` (
  `ID` int(11) NOT NULL,
  `EMAIL` varchar(255) DEFAULT NULL,
  `PASSWORD` varchar(255) DEFAULT NULL,
  `TOKEN` tinyblob,
  `EXPIREDATE` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `TOKEN` (
  `ID` int(11) NOT NULL,
  `USERID` int(11) DEFAULT NULL,
  `DATE` timestamp DEFAULT NULL,
  `EXPIREDATE` timestamp DEFAULT NULL,
  `VALUE` tinyblob,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `SEQUENCE` (
  `SEQ_NAME` varchar(50) NOT NULL,
  `SEQ_COUNT` decimal(38,0) DEFAULT NULL,
  PRIMARY KEY (`SEQ_NAME`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

INSERT INTO `SEQUENCE` VALUES ('SEQ_GEN_TABLE',1200);
INSERT INTO `USER` VALUES (1,'fpi@bk.ru','123123','','2016-09-27 16:34:05');
```


# DIAGRAM #

![diagram.png](https://bitbucket.org/repo/Bn9doa/images/1950183386-diagram.png)