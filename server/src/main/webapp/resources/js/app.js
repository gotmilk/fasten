var socket = null;

function setConnected(connected) {

	if (connected) {
		$("#connect").text("Disconnect");
	} else {
		$("#connect").text("Connect");
	}

	$('#response').val("");
}

function appendResponse(text) {
	$('#response').val(function(_, val) {
		return val + text;
	});
}

function send(json_to_send) {
	if (socket) {
		socket.send(json_to_send);
	}
}

function connect(url) {

	if (socket) {
		disconnect();
		return;
	}

	socket = new WebSocket(url);

	socket.onopen = function() {
		setConnected(true);
		console.log("Connected\r\n")
	};

	socket.onerror = function(event) {
		console.log(event);
		setConnected(false);
		socket = null;
	};

	socket.onmessage = function(event) {
		appendResponse(event.data);
		console.log(event);
	};

	socket.onclose = function(event) {
		console.log(event.code + " : " + event.reason);
		setConnected(false);
		socket = null;
	};

}

function disconnect() {

	if (socket) {
		socket.close();
	}
	setConnected(false);
	console.log("Disconnected\r\n");
}

$(function() {
	$("#send").click(function() {
		$('#response').val("");
		var text = $('#request').val();
		send(text);
	});
});