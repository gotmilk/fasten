<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
    <%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="sf"%>
    <%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Active Users</title>
<link
	href="<spring:url value="/resources/bootstrap/css/bootstrap.min.css"/>"
	rel="stylesheet" />
<link href="<spring:url value="/resources/css/main.css"/>"
	rel="stylesheet" />
<script src="<spring:url value="/resources/js/jquery-3.1.1.min.js"/>"></script>
</head>
<body>

<div class="container">
  <h2>Active Users</h2>
  <p>Shows currently active users:</p>
  <table class="table table-hover">
    <thead>
      <tr>
        <th>Date</th>
        <th>Hostname</th>
        <th>Session ID</th>
        <th>Socket ID</th>
      </tr>
    </thead>
    <tbody>
    <c:forEach items="${users}" var="user">
      <tr>
      	<td>${user.connectionTime}</td>
      	<td>${user.hostname}</td>
        <td>${user.id}</td>
        <td>${user.socketId}</td>
      </tr>
      </c:forEach>
    </tbody> 
  </table>
</div>
<script>
function startRefresh() {
    $.get('', function(data) {
        $(document.body).html(data);    
    });
}
$(function() {
    setTimeout(startRefresh,3000);
});
</script>
</body>
</html>