<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="sf"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
<head>
<title>Hello WebSocket</title>
<link
	href="<spring:url value="/resources/bootstrap/css/bootstrap.min.css"/>"
	rel="stylesheet" />
<link href="<spring:url value="/resources/css/main.css"/>"
	rel="stylesheet" />
<script src="<spring:url value="/resources/js/jquery-3.1.1.min.js"/>"></script>
<script src="<spring:url value="/resources/js/sockjs-0.3.min.js"/>"></script>
<script src="<spring:url value="/resources/js/app.js"/>"></script>
</head>
<body>
	<noscript>
		<h2 style="color: #ff0000">Seems your browser doesn't support
			Javascript! Websocket relies on Javascript being enabled. Please
			enable Javascript and reload this page!</h2>
	</noscript>
<c:set var="wsurl" value="ws://${pageContext.request.serverName}:${pageContext.request.serverPort}${pageContext.request.contextPath}/auth"/>
	<h2>
		Simple WebSocket client
		<button id="connect" type="submit" class="btn btn-primary pull-right" onclick="connect('${wsurl}')">Connect</button>
	</h2>
	<hr/>
	<div class="container">
		<div class="row clearfix">
			<div class="col-md-12 column">

				<div class="form-group">
					<label for="request">Request</label>
					<textarea id="request" class="txtarea form-control col-xs-12" rows="7"></textarea>
				</div>

				<div class="form-group txtarea">
					<button id="send" class="txtarea btn btn-primary">Submit</button>
				</div>

				<div class="form-group txtarea">
					<label for="response">Response</label>
					<textarea id="response" class="txtarea form-control col-xs-12" rows="7">null</textarea>
				</div>
			</div>
		</div>
	</div>
</body>
</html>