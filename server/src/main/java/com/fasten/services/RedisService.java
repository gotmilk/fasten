package com.fasten.services;

import java.util.concurrent.TimeUnit;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Service;

@Service
public class RedisService<T> {

	@Autowired
    private RedisTemplate< String, Object > redisTemplate;

	@SuppressWarnings("unchecked")
    public T getValue(String key) {
        return (T)redisTemplate.opsForValue().get(key);
    }

    public void setValue(String key, T value) {
    	redisTemplate.opsForValue().set(key, value);
    	redisTemplate.expire(key, 2, TimeUnit.DAYS);
    }
}
