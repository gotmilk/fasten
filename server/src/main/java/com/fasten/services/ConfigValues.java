package com.fasten.services;

import java.util.UUID;

import org.springframework.stereotype.Component;

@Component("config")
public class ConfigValues {

	public static final String nodeName = UUID.randomUUID().toString();

	public static String getNodeName() {
		return nodeName;
	}
	
}
