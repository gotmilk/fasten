package com.fasten.messaging;

import java.io.IOException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jms.annotation.JmsListener;
import org.springframework.stereotype.Component;

import com.fasten.messages.GlobalServerMessage;
import com.fasten.websocket.WebSocketHandler;
import com.fasterxml.jackson.databind.ObjectMapper;

@Component
public class AuthServerMessageListener {

	private static final Logger logger = LoggerFactory.getLogger(AuthServerMessageListener.class);

	@Autowired
	private WebSocketHandler webSocketHandler;
	
	@JmsListener(destination = "${auth.topic}", selector = "node='#{config.nodeName}'", containerFactory = "topicJmsListenerContainerFactory")
	public void processTopicMessage(String content) {
		logger.info("Received topic message.  Content is " + content);

		GlobalServerMessage serverMessage = null;
		ObjectMapper mapper = new ObjectMapper();

		try {
			serverMessage = mapper.readValue(content, GlobalServerMessage.class);

			webSocketHandler.sendSocketMessage(serverMessage.getSessionId(), serverMessage.getMsg());
		} catch (IOException e) {
			logger.info("Could not send socket message from topic..." + e);
		}
	}
}
