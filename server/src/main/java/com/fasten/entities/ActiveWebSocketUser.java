package com.fasten.entities;

import java.io.Serializable;
import java.util.Date;

public class ActiveWebSocketUser implements Serializable
{
	private static final long serialVersionUID = 3176794543708856978L;
	
	private String id;
	private String socketId;
	private Date connectionTime;
	private String hostname;
	
	
	public String getHostname() {
		return hostname;
	}

	public void setHostname(String hostname) {
		this.hostname = hostname;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public ActiveWebSocketUser() {
	}

	public ActiveWebSocketUser(String id, String socketId, Date connectionTime) {
		super();
		this.id = id;
		this.socketId = socketId;
		this.connectionTime = connectionTime;
	}

	public String getSocketId() {
		return this.socketId;
	}

	public void setSocketId(String socketId) {
		this.socketId = socketId;
	}

	public Date getConnectionTime() {
		return this.connectionTime;
	}

	public void setConnectionTime(Date connectionTime) {
		this.connectionTime = connectionTime;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		ActiveWebSocketUser other = (ActiveWebSocketUser) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}

	
}
