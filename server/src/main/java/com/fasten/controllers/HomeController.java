package com.fasten.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.fasten.websocket.WebSocketHandler;

@Controller
public class HomeController {
	
	@Autowired
	private WebSocketHandler webSocketHandler;
	
	@RequestMapping(value="/", method=RequestMethod.GET)
	public String home() {
		return "home";
	}
	
	@RequestMapping(value="/active", method=RequestMethod.GET)
	public String connected(Model model) {
		model.addAttribute("users", webSocketHandler.listActiveUsers());
		return "active";
	}

}
