package com.fasten.config;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.context.annotation.PropertySource;
import org.springframework.context.support.PropertySourcesPlaceholderConfigurer;
import org.springframework.data.redis.connection.jedis.JedisConnectionFactory;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.serializer.GenericToStringSerializer;
import org.springframework.data.redis.serializer.Jackson2JsonRedisSerializer;
import org.springframework.data.redis.serializer.StringRedisSerializer;
import org.springframework.transaction.annotation.EnableTransactionManagement;

import com.fasten.entities.ActiveWebSocketUser;

@Configuration
@PropertySource(value = { "classpath:main.properties" })
@ComponentScan(basePackages = { 
		"com.fasten.services", "com.fasten.repositories", "com.fasten.messaging", "com.fasten.websocket","com.fasten.utils" })
@Import({ MessagingConfig.class, MessagingListnerConfig.class })
@EnableTransactionManagement
public class AppConfig {
	
	@Value("${redis.host}")
	private String redisHost;

	@Value("${redis.password}")
	private String redisPassword;

	@Value("${redis.port}")
	private int redisPort;

	
	@Bean
	public static PropertySourcesPlaceholderConfigurer propertyConfigIn() {
		return new PropertySourcesPlaceholderConfigurer();
	}

	@Bean
	public JedisConnectionFactory jedisConnectionFactory() {
		JedisConnectionFactory redisConnectionFactory = new JedisConnectionFactory();
		redisConnectionFactory.setHostName(redisHost);
		redisConnectionFactory.setPort(redisPort);
		if (redisPassword != null)
			redisConnectionFactory.setPassword(redisPassword);
		redisConnectionFactory.setUsePool(true);
		
		return redisConnectionFactory;
	}

	@Bean
	public RedisTemplate<String, Object> redisTemplate() {
		final RedisTemplate<String, Object> template = new RedisTemplate<String, Object>();

		try {
			template.setConnectionFactory(jedisConnectionFactory());
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		template.setConnectionFactory(jedisConnectionFactory());
		template.setKeySerializer(new StringRedisSerializer());
		template.setHashValueSerializer(new GenericToStringSerializer<Object>(Object.class));
		template.setValueSerializer(new Jackson2JsonRedisSerializer<ActiveWebSocketUser>(ActiveWebSocketUser.class));
		return template;
	}
	
}
