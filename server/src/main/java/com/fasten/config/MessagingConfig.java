package com.fasten.config;

import org.apache.activemq.ActiveMQConnectionFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.jms.core.JmsTemplate;

@Configuration
public class MessagingConfig {

	@Value("${broker.url}")
	private String brokerUrl;

	@Value("${auth.queue}")
	private String authQueueName;
	
	@Value("${auth.topic}")
	private String authTopicName;
	
	@Value("${broker.trust.all:false}")
	private boolean trustAllPackages;

	@Bean
	public ActiveMQConnectionFactory connectionFactory() {
		ActiveMQConnectionFactory connectionFactory = new ActiveMQConnectionFactory();
		connectionFactory.setBrokerURL(brokerUrl);
//		connectionFactory.setWatchTopicAdvisories(false);
		/*
		if (trustAllPackages)
			connectionFactory.setTrustAllPackages(true);
		else
			connectionFactory.setTrustedPackages(Arrays.asList("com.fasten"));
			*/
		return connectionFactory;
	}


	@Bean
	public JmsTemplate jmsQueueTemplate() {
		JmsTemplate template = new JmsTemplate();
		template.setConnectionFactory(connectionFactory());
		template.setDefaultDestinationName(authQueueName);
		return template;
	}
	
	@Bean
	public JmsTemplate jmsTopicTemplate() {
		JmsTemplate template = new JmsTemplate();
		template.setConnectionFactory(connectionFactory());
		template.setDefaultDestinationName(authTopicName);
		template.setPubSubDomain(true);
		return template;
	}

}
