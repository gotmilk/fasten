package com.fasten.websocket;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.UUID;
import java.util.concurrent.ConcurrentHashMap;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import org.springframework.web.socket.CloseStatus;
import org.springframework.web.socket.TextMessage;
import org.springframework.web.socket.WebSocketSession;
import org.springframework.web.socket.handler.TextWebSocketHandler;

import com.fasten.entities.ActiveWebSocketUser;
import com.fasten.messages.GlobalServerMessage;
import com.fasten.messages.ServerMessage;
import com.fasten.messages.ServerMessageErrorData;
import com.fasten.messages.ServerMessageTypes;
import com.fasten.messaging.AuthServerMessageQueueSender;
import com.fasten.services.RedisService;
import com.fasten.utils.JsonUtils;

@Component
public class WebSocketHandler extends TextWebSocketHandler {

	private static final Map<String, String> internalSessions = new ConcurrentHashMap<>();
	private static final Map<String, WebSocketSession> sockets = new ConcurrentHashMap<>();
	
	@Value("#{config.nodeName}")
	private String nodeName;
	
	@Autowired
	private AuthServerMessageQueueSender queue;
	
	@Autowired
	private JsonUtils jsonUtils;
	
	@Autowired
	private RedisService<ActiveWebSocketUser> redisService;

	private static final Logger logger = Logger.getLogger(WebSocketHandler.class);
	
	public List<ActiveWebSocketUser> listActiveUsers() {
		
		List<ActiveWebSocketUser> list = new ArrayList<>();
		
		for (Map.Entry<String, String> entry : internalSessions.entrySet())
		{
			ActiveWebSocketUser user = redisService.getValue(entry.getValue());
			list.add(user);
		}
		return list;
	}
	
	public void sendSocketMessage(String sessionId, Object o) {
		try {
			// Check if Redis contains sessionId
			ActiveWebSocketUser user = redisService.getValue(sessionId);

			if (user == null)
				return;

			String socketId = user.getSocketId();
			WebSocketSession sock = sockets.get(socketId);

			if (sock != null) {
				if (o instanceof ServerMessage) {
					String json = jsonUtils.serializeJson(o);
					if (json != null) 
					sock.sendMessage(new TextMessage(json));
				} else {
					sock.sendMessage(new TextMessage(o.toString()));
				}
			}

		} catch (Exception e) {
			logger.error("Error occured sending to " + sessionId, e);
		}
	}

	@Override
	public void handleTransportError(WebSocketSession session, Throwable throwable) throws Exception {
		logger.error("error occured at sender " + session, throwable);
	}

	@Override
	public void afterConnectionClosed(WebSocketSession session, CloseStatus status) throws Exception {
		logger.info(String.format("Session %s closed because of %s", session.getId(), status.getReason()));
		// Remove UserSession object from the storage
		internalSessions.remove(session.getId());
		sockets.remove(session.getId());
		
		// The key will be removed automatically by Redis
	}

	@Override
	public void afterConnectionEstablished(WebSocketSession session) throws Exception {
		logger.info("Connected ... " + session.getId());
		// Save current user to Map
		sockets.put(session.getId(), session);

		// Save User info to Redis
		String userSession = UUID.randomUUID().toString();
		internalSessions.put(session.getId(), userSession);
		
		ActiveWebSocketUser user = new ActiveWebSocketUser(userSession, session.getId(), new Date());
		user.setHostname(session.getRemoteAddress().getHostName() + ":" + session.getRemoteAddress().getPort());
		redisService.setValue(userSession, user);
			
	}

	@Override
	protected void handleTextMessage(WebSocketSession session, TextMessage jsonTextMessage) throws Exception {

		String payload = jsonTextMessage.getPayload();
		logger.info("message received: " + payload);

		ServerMessage msg = (ServerMessage)jsonUtils.deserializeJson(payload, ServerMessage.class);
		
		if (msg == null) {
			
			msg = new ServerMessage();
			msg.setType(ServerMessageTypes.CUSTOMER_ERROR);
			ServerMessageErrorData data = new ServerMessageErrorData();
			data.setErrorCode("customer.notFound");
			data.setErrorDescription("Customer not found");
			msg.setData(data);
			
			String json = jsonUtils.serializeJson(msg);
			session.sendMessage(new TextMessage(json));
			return;
		}
		
		logger.info("message type: " + msg.getType());
		
		GlobalServerMessage gMsg = new GlobalServerMessage();
		ServerMessage myMsg = new ServerMessage();
		myMsg.setType(msg.getType());
		myMsg.setSequenceId(msg.getSequenceId());
		myMsg.setData(msg.getData());
		gMsg.setMsg(myMsg);
		
		gMsg.setSessionId(internalSessions.get(session.getId()));
	
		String result = jsonUtils.serializeJson(gMsg);
		queue.sendMessage(result, nodeName);
	}

}
