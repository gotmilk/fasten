package com.fasten;

import java.io.IOException;
import java.util.Map;
import java.util.UUID;

import org.junit.Assert;
import org.junit.Test;

import com.fasten.messages.ServerMessage;
import com.fasten.messages.ServerMessageErrorData;
import com.fasten.messages.ServerMessageLoginData;
import com.fasten.messages.ServerMessageTypes;
import com.fasterxml.jackson.databind.ObjectMapper;

public class PolymorphicDeserializationTests {

	private static final String LOGIN_CUSTOMER_JSON = "{\"type\":\"LOGIN_CUSTOMER\",\"sequence_id\":\"a29e4fd0-581d-e06b-c837-4f5f4be7dd18\",\"data\":{\"email\":\"fpi@bk.ru\",\"password\":\"123123\"}}";
	private static final String DATA_ERROR_JSON = "{\"type\":\"CUSTOMER_ERROR\",\"sequence_id\":\"715c13b3-881a-9c97-b853-10be585a9747\",\"data\":{\"error_description\":\"Customer not found\",\"error_code\":\"customer.notFound\"}}";

	@Test
	public void deserializeLoginJson() {
		ServerMessage msg = deserializeJson(LOGIN_CUSTOMER_JSON);
		Assert.assertTrue(msg.getData() instanceof ServerMessageLoginData);

		if (msg.getData() instanceof ServerMessageLoginData) {
			ServerMessageLoginData data = (ServerMessageLoginData) msg.getData();
			Assert.assertEquals(data.getEmail(), "fpi@bk.ru");
			Assert.assertEquals(data.getPassword(), "123123");
		}
	}

	@Test
	public void serializeLoginJson() {
		ServerMessage sm = new ServerMessage();
		sm.setType(ServerMessageTypes.LOGIN_CUSTOMER);
		ServerMessageLoginData smData = new ServerMessageLoginData();
		smData.setEmail("fpi@bk.ru");
		smData.setPassword("123123");
		sm.setData(smData);
		sm.setSequenceId(UUID.fromString("a29e4fd0-581d-e06b-c837-4f5f4be7dd18"));

		String result = serializeJson(sm);
		Assert.assertEquals(jsonEquals(result, LOGIN_CUSTOMER_JSON), true);
	}

	@Test
	public void serializeErrorJson() {
		ServerMessage sm = new ServerMessage();
		sm.setType(ServerMessageTypes.CUSTOMER_ERROR);
		ServerMessageErrorData smData = new ServerMessageErrorData();
		smData.setErrorCode("customer.notFound");
		smData.setErrorDescription("Customer not found");
		sm.setData(smData);
		sm.setSequenceId(UUID.fromString("715c13b3-881a-9c97-b853-10be585a9747"));

		String result = serializeJson(sm);
		Assert.assertEquals(jsonEquals(result, DATA_ERROR_JSON), true);
	}

	private ServerMessage deserializeJson(String json) {
		ObjectMapper mapper = new ObjectMapper();
		try {
			return mapper.readValue(json, ServerMessage.class);
		} catch (IOException e) {
			Assert.fail("Could not deserialize JSON: " + e);
		}
		return null;
	}

	private String serializeJson(ServerMessage message) {
		ObjectMapper mapper = new ObjectMapper();
		try {
			return mapper.writeValueAsString(message);
		} catch (IOException e) {
			Assert.fail("Could not serialize JSON: " + e);
		}
		return null;
	}

	@SuppressWarnings("unchecked")
	private boolean jsonEquals(String json1, String json2) {
		ObjectMapper om = new ObjectMapper();
		try {
			Map<String, Object> m1 = (Map<String, Object>) (om.readValue(json1, Map.class));
			Map<String, Object> m2 = (Map<String, Object>) (om.readValue(json2, Map.class));
			return m1.equals(m2);
		} catch (Exception e) {
			Assert.fail("Incorrect JSON.." + e);
		}
		return false;
	}

}
