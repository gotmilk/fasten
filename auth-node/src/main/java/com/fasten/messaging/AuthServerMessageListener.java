package com.fasten.messaging;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jms.annotation.JmsListener;
import org.springframework.messaging.handler.annotation.Header;
import org.springframework.stereotype.Component;

import com.fasten.entities.User;
import com.fasten.messages.GlobalServerMessage;
import com.fasten.messages.ServerMessage;
import com.fasten.messages.ServerMessageData;
import com.fasten.messages.ServerMessageErrorData;
import com.fasten.messages.ServerMessageLoginData;
import com.fasten.messages.ServerMessageTokenData;
import com.fasten.messages.ServerMessageTypes;
import com.fasten.services.AuthService;
import com.fasten.utils.JsonUtils;

@Component
public class AuthServerMessageListener {

	private static final Logger logger = LoggerFactory.getLogger(AuthServerMessageListener.class);

	@Autowired
	private AuthServerMessagePublisher authServerMessagePublisher;
	
	@Autowired
	private AuthService authService;
	
	@Autowired
	private JsonUtils jsonUtils;

	// Customize with @Header
	@JmsListener(destination = "${auth.queue}")
	public void processQueueMessage(@Header(name="node", defaultValue="undef") String serverName, String content) {
		logger.info("Received queue message.  Content is " + content);
		logger.info("From node: " + serverName);

		// Read JSON and get message attribute
		GlobalServerMessage gMsg = (GlobalServerMessage) jsonUtils.deserializeJson(content,
				GlobalServerMessage.class);

		if (gMsg == null) {
			// Internal error can't parse JMS JSON
			logger.error("Can't parse internal JSON in a QUEUE");
			return;
		}

		ServerMessage response = new ServerMessage();
		response.setSequenceId(gMsg.getMsg().getSequenceId());
		
		String email = null;
		String password = null;
		User user = null;
		ServerMessageData smld = null;
				
		// The password should be hashed of course
		// Use this at your own risk (don't store password in open format)
		if (gMsg.getMsg().getType().equals(ServerMessageTypes.LOGIN_CUSTOMER)) {
			ServerMessageLoginData loginData = (ServerMessageLoginData)gMsg.getMsg().getData();
			email = loginData.getEmail();
			password = loginData.getPassword();
			user = authService.auth(email, password);
		}
		
		if (user != null) {
			response.setType(ServerMessageTypes.CUSTOMER_API_TOKEN);
			ServerMessageTokenData tokenData = new ServerMessageTokenData();
			
			tokenData.setApiToken(user.getToken());
			tokenData.setApiTokenExpirationDate(user.getExpireDate());
			smld = tokenData;
		} else {
			response.setType(ServerMessageTypes.CUSTOMER_ERROR);
			ServerMessageErrorData errorData = new ServerMessageErrorData();
			errorData.setErrorCode("customer.notFound");
			errorData.setErrorDescription("Customer not found");
			smld = errorData;
		}
		
		response.setData(smld);
		gMsg.setMsg(response);
		
		String json = jsonUtils.serializeJson(gMsg); 
		
		
		authServerMessagePublisher.sendMessage(json, serverName);
	}
}
