package com.fasten.services;

import java.util.Date;
import java.util.UUID;

import javax.transaction.Transactional;

import org.apache.commons.lang3.time.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import com.fasten.entities.User;
import com.fasten.entities.Token;
import com.fasten.repositories.UserRepository;
import com.fasten.repositories.TokenRepository;

@Service
@Transactional
public class MyAuthService implements AuthService {

	private UserRepository userRepository;
	private TokenRepository tokenRepository;
	
	@Value("${expire.hour:4}")
	private int tokenExpireHours;
	
	@Autowired
	public MyAuthService(UserRepository userRepository,
			TokenRepository tokenRepository) {
		this.userRepository = userRepository;
		this.tokenRepository = tokenRepository;	
	}

	@Override
	public User auth(String user, String password) {
		
		User account = userRepository.findByEmail(user);
		
		if (account == null) {
			return account;
		} 
		
		if (!account.getPassword().equals(password)) {
			return null;
		}
		
		// Saving token to Repository
		Token token = new Token();
		token.setAccountId(account.getId());
		token.setDate(new Date());
		token.setValue(account.getToken());
		token.setExpireDate(account.getExpireDate());
		tokenRepository.save(token);
		
		// Generating new token for account && saving
		UUID tokenUUID = UUID.randomUUID();
		account.setToken(tokenUUID);
		Date expireDate = new Date();
		expireDate = DateUtils.addHours(expireDate, tokenExpireHours);
		account.setExpireDate(expireDate);
		userRepository.update(account);
		
		return account;
	}
	
}
