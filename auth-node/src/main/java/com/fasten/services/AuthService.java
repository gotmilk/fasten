package com.fasten.services;

import com.fasten.entities.User;

public interface AuthService {
	User auth(String email, String password);
}
