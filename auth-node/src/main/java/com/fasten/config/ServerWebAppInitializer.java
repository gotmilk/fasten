package com.fasten.config;

import org.springframework.web.servlet.support.AbstractAnnotationConfigDispatcherServletInitializer;

public class ServerWebAppInitializer extends AbstractAnnotationConfigDispatcherServletInitializer {

	@Override
	protected Class<?>[] getRootConfigClasses() {
		return new Class<?>[] { AppConfig.class };
	}

	@Override
	protected Class<?>[] getServletConfigClasses() {
//		return new Class<?>[] { WebConfig.class };
		return null;
	}

	
	@Override
	protected String[] getServletMappings() {
//		return new String[] { "/auth-node" };
		return null;
	}

}
