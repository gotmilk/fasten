package com.fasten.repositories;

import java.util.List;

import com.fasten.entities.Token;

public interface TokenRepository {
	void save(Token token);
	List<Token> getAll();
}
