package com.fasten.repositories;

import com.fasten.entities.User;

public interface UserRepository {
	int create(User account);
    void update(User account);
    User findByEmail(String email);
    User findById(Long id);
    void delete(Long id);
}
