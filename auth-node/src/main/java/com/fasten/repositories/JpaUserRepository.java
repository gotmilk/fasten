package com.fasten.repositories;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.PersistenceException;
import javax.transaction.Transactional;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Repository;

import com.fasten.entities.User;

@Repository
@Transactional
public class JpaUserRepository implements UserRepository {
	
	private static final Logger logger = Logger.getLogger(JpaUserRepository.class);

	@PersistenceContext
	private EntityManager entityManager;

	@Override
	public int create(User account) {
		entityManager.persist(account);
		return account.getId();
	}

	@Override
	public void update(User account) {
		entityManager.merge(account);
	}

	@Override
	public User findByEmail(String email) {
		User result = null;
		try {
			result = entityManager.createQuery("SELECT e FROM User e WHERE e.email = :email", User.class)
					.setParameter("email", email).getSingleResult();

		} catch (PersistenceException e) {
			logger.error("Error while retrieving the user", e);
		}
		
		return result;
	}

	@Override
	public User findById(Long id) {
		return entityManager.find(User.class, id);
	}

	@Override
	public void delete(Long id) {
		entityManager.remove(findById(id));
	}

}
