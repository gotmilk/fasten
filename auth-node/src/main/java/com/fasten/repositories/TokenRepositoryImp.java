package com.fasten.repositories;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.Transactional;

import org.springframework.stereotype.Repository;

import com.fasten.entities.Token;

@Repository
@Transactional
public class TokenRepositoryImp implements TokenRepository {

	@PersistenceContext
	private EntityManager entityManager;
	
	@Override
	public void save(Token token) {
		entityManager.persist(token);
	}

	@Override
	public List<Token> getAll() {
		return entityManager.createQuery("SELECT a FROM Token a", Token.class).getResultList();
	}
 
}
